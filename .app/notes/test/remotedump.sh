#!/bin/bash
source ~/workspace/.app/site/_appdata.io;

: '
    CHANGE PRODUCTION TO STAGING DIR AND URL:
    
    login="hello.istria@gmail.com"
    password="helloistria"
    appname="hello"
    gitsource="git@bitbucket.org:c9cube/quickstart.git"
    giturl="ssh://56de947a0c1e665d8300009c@hello-istria.rhcloud.com/~/git/hello.git/"
    ssh="56de947a0c1e665d8300009c@hello-istria.rhcloud.com"
    user="56de947a0c1e665d8300009c"
    site="hello-istria.rhcloud.com"
    http://hello-istria.rhcloud.com
    https://helloistria-infocube.c9users.io
'
#production dump
ssh -t $ssh << 'ENDSSH'
mysqldump -h $OPENSHIFT_MYSQL_DB_HOST -P ${OPENSHIFT_MYSQL_DB_PORT:-3306} \
-u ${OPENSHIFT_MYSQL_DB_USERNAME:-'admin'} --password="$OPENSHIFT_MYSQL_DB_PASSWORD" \
--databases $OPENSHIFT_APP_NAME > /var/lib/openshift/56de947a0c1e665d8300009c/app-root/runtime/repo/wp-content/mysqldump/production.sql
ENDSSH

dbname=$appname
mysql-ctl cli << MYSQL
DROP DATABASE $dbname;
CREATE DATABASE $dbname;
MYSQL

cp ~/mounts/haloistra/wp-content/mysqldump/production.sql ~/workspace/.app/notes/test

production_dir="\/var\/lib\/openshift\/"
production_dir+=$user
production_dir+="\/app-root\/runtime\/repo"
staging_dir="\/home\/ubuntu\/workspace"

production_url=$site
project=$C9_PROJECT
staging_url+=$project
staging_url+="-infocube.c9users.io"

sed -i "s/$production_dir/$staging_dir/g" ~/workspace/.app/notes/test/production.sql
sed -i "s/$production_url/$staging_url/g" ~/workspace/.app/notes/test/production.sql
