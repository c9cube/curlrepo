#space on node
du -hc
#biggest 100 files/folders
du -hc | sort -n -r | head -n 100
#just total
du -hcs
#pokušaj malo počistiti
rhc app-tidy
#kako stoji openshift
df -h
#set deployments number to 1
rhc deployment list -a $appname
rhc app-configure $appname --keep-deployments 1
#delete app
no way -to treba napraviti tamo-
#stop apache
sudo /etc/init.d/apache2 stop
ili
sudo service apache2 stop
#start apache
sudo /etc/init.d/apache2 start
ili
sudo service apache2 start
#ovo ponekad pomaže, kada visi
rhc app-force-stop $appname
rhc app-tidy $appname
rhc app-start $appname
#about app
rhc app-show -v $appname

#domene, ovako nešto treba nadodati u wp-config, kada su u igri domene
if (getenv('OPENSHIFT_APP_NAME') != "") {
	define('WP_HOME','http://hello-istria.rhcloud.com');
	define('WP_SITEURL','http://hello-istria.rhcloud.com');
} 
else {
	$SITE=basename(dirname(__FILE__));
	$PROJECT=getenv('C9_PROJECT');
	$NAME='https://'.$PROJECT.'-infocube.c9users.io/'.$SITE.'/';
	define('WP_HOME', $NAME);
	define('WP_SITEURL', $NAME);
}
/*Custom domain*/
if (getenv('OPENSHIFT_APP_NAME') != "") {
	define('WP_HOME','http://dreambox.ninja');
	define('WP_SITEURL','http://dreambox.ninja');
} 
else {
	$SITE=basename(dirname(__FILE__));
	$PROJECT=getenv('C9_PROJECT');
	$NAME='https://'.$PROJECT.'-infocube.c9users.io/'.$SITE.'/';
	define('WP_HOME', $NAME);
	define('WP_SITEURL', $NAME);
}


#+add domain alias !!!! inače dobiješ redirect loop (ok, barem znam, da bez aliasa ne ide...)
rhc alias add <application name> <custom domain name>


#TO JE TO!
#upload
rsync -avz ~/workspace/hello/wp-content/ ~/mounts/helloistria/wp-content
rsync -avz ~/workspace/hello/wp-content/plugins/ ~/mounts/helloistria/wp-content/plugins
rsync -avz ~/workspace/hello/wp-content/themes/ ~/mounts/helloistria/wp-content/themes

#download
rsync -avz ~/mounts/helloistria/wp-content/ ~/workspace/hello/wp-content 
rsync -avz ~/mounts/helloistria/wp-content/plugins/ ~/workspace/hello/wp-content/plugins
rsync -avz ~/mounts/helloistria/wp-content/themes/ ~/workspace/hello/wp-content/themes

rsync -avz ~/workspace/.app/notes/test/f1/plugins/ ~/workspace/.app/notes/test/f2/plugins


