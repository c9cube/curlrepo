#!/bin/sh

#u stvari ovo bi trebalo zapisati u staging file i njega kopirati gore
#a nakon toga napraviti push sa staginga
#plus toga možda bi trebalo iči sve to u osnovni instalacijski file

#ovo treba sada preurediti, sve če iči preko rsync ssh-ja
read -p "Ako si kreirao mount samo pritisni enter (a inače ga prije kreiraj):"

source ~/workspace/.app/site/_appdata.io;

#idi na mount
cd ~/workspace/$appname;

#ok, sada mi treba samo još domena
read -p "Molim unesi golu domenu (bez http//:) i pritisni enter: " domena

sed -i "31i\\
if (getenv('OPENSHIFT_APP_NAME') != \"\") {\\
	define('WP_HOME','http://"$domena"');\\
	define('WP_SITEURL','http://"$domena"');\\
}\\
else {\\
	\$SITE=basename(dirname(__FILE__));\\
	\$PROJECT=getenv('C9_PROJECT');\\
	\$NAME='https://'.\$PROJECT.'-infocube.c9users.io/'.\$SITE.'/';\\
	define('WP_HOME', \$NAME);\\
	define('WP_SITEURL', \$NAME);\\
}\\
" wp-config.php

#cp -f ~/workspace/$appname/wp-config.php ~/mounts/$appname/wp-config.php;

rhc alias add $appname $domena

echo ""
echo ":)"
