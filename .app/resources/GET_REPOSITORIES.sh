#!/bin/bash
curl --user infocube.dev@gmail.com:volimtedoneba https://api.bitbucket.org/2.0/repositories/c9cube \
| JSON.sh | awk '/"links","clone",1,"href"/' \
| sed 's/\[[^][]*\]//g' \
| sed 's/\"//g' \
| sed 's/[[:blank:]]//g' > ~/workspace/curlrepo/.app/resources/repositories.txt;